mov r1, v0
//invert vertex
mov r1.x, <negate>r1.x
//set vertex.w = 0
add r1.w, c0.x <negate>c0.x
//vertex * ViewProjection matrix
dp4 r2.x, r1, c119
dp4 r2.y, r1, c120
dp4 r2.w, r1, c122
//set vertex.z to vertex.w
mov r3.x c2.y
mul r3.x, r3.x, r3.x
add r2.z r2.w, <negate>r3.x

//write transformed vertex to output
mov o0, r2
//set o7 to vertex position
mov o7, r1

//set all other outputs to zero
mov o1, c0.y
mov o2.x, c0.y
mov o2.y, c0.y
mov o2.z, c0.y
mov o3.x, c0.y
mov o3.y, c0.y
mov o3.z, c0.y
mov o4.x, c0.y
mov o4.y, c0.y
mov o4.z, c0.y
mov o5.x, c0.y
mov o5.y, c0.y
mov o5.z, c0.y
mov o6.xy, v4
mov o6.zw, c0.y
mov o8, c0.y